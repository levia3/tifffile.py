web pages
-------

* [tifffile.py], [tifffile.c] ... [Christoph Gohlke's]
  [Christoph Gohlke's]: http://www.lfd.uci.edu/~gohlke/
  [tifffile.py]: http://www.lfd.uci.edu/~gohlke/code/tifffile.py.html
  [tifffile.c]: http://www.lfd.uci.edu/~gohlke/code/tifffile.c.html

* [write.c], [read.c] ... [IBM developerWorks]
  [IBM developerWorks]: http://www.ibm.com/developerworks
  [write.c]: http://www.ibm.com/developerworks/linux/library/l-libtiff/write.c
  [read.c]: http://www.ibm.com/developerworks/linux/library/l-libtiff/read.c

* [tiffcmp] ... [Using The TIFF Library]
  [Using The TIFF Library]: http://www.remotesensing.org/libtiff/libtiff.html
  [tiffcmp]: http://www.remotesensing.org/libtiff/man/tiffcmp.1.html

* [convert], [drawing], [compare] ... [ImageMagick]
  [ImageMagick]: http://www.imagemagick.org/
  [convert]: http://www.imagemagick.org/script/convert.php
  [drawing]: http://www.imagemagick.org/Usage/draw
  [compare]: http://www.imagemagick.org/script/compare.php

* [Programming Computer Vision with Python] ---
  [samples description (Japanese)], [book(JP)], [pcv_data], [draft pdf], [pcv GitHub]
  [Programming Computer Vision with Python]: http://programmingcomputervision.com/
  [draft pdf]: http://programmingcomputervision.com/downloads/ProgrammingComputerVision_CCdraft.pdf
  [pcv GitHub]: https://github.com/jesolem/PCV
  [book(JP)]: http://www.oreilly.co.jp/books/9784873116075/#toc
  [samples description (Japanese)]: http://www.oreilly.co.jp/pub/9784873116075/index.html
  [pcv_data]: http://programmingcomputervision.com/downloads/pcv_data.zip

* [2.4.8 Reference manual] ... [OpenCV Documentation]
  [OpenCV Documentation]: http://opencv.org/documentation.html
  [2.4.8 Reference manual]: http://docs.opencv.org/2.4.8/modules/refman.html

samples
-------
* [01] ... use tiffcmp
* [02] ... use tifffile
* [03] ... analyze tif
* [04] ... read 1 strip
* [05] ... read 1 tile
* [06] ... read BW image
  [01]:  #markdown-header-01-use-tiffcmp
  [02]: #markdown-header-02-use-tifffile
  [03]: #markdown-header-03-analyze-tif
  [04]: #markdown-header-04-read-1-strip
  [05]: #markdown-header-05-read-1-tile
  [06]: #markdown-header-06-read-bw-image

### 01 ... use tiffcmp
```sh
#!/bin/sh

src="empire.jpg"   # ... in pcv_data
tif_a="a.tif"
tif_b="b.tif"

cv2_path="/usr/local/opencv/lib/python2.7/dist-packages"
python -c 'if __name__ == "__main__" :
    import sys ; import numpy as np
    sys.path.append( "'"$cv2_path"'" ); import cv2 ; import cv2.cv as cv

    img_a = cv2.imread( "'"$src"'" )
    img_b = cv2.imread( "'"$src"'" )
    img_b[ 10, 20, : ] = [ 5, 6, 7 ]

    cv2.imwrite( "'"$tif_a"'", img_a )
    cv2.imwrite( "'"$tif_b"'", img_b )
'

tiffcmp -l -z10 "$tif_a" "$tif_b"
```
```
Scanline 10, pixel 20, sample 0: 57 07
Scanline 10, pixel 20, sample 1: 84 06
Scanline 10, pixel 20, sample 2: bb 05
```

### 02 ... use tifffile
* setup ... _tifffile.so
```sh
fn="tifffile"
opt_py="-I$( python -c 'from distutils.sysconfig import * ; print get_python_inc()' )"
opt_np="-I$( python -c 'import numpy ; print numpy.get_include()' )"
gcc -o _$fn.so $fn.c -shared -fPIC $opt_py $opt_np
```
* with pyplot
```sh
#!/bin/sh

src="empire.jpg"   # ... in pcv_data
tif="empire.tif"

convert "$src" -compress zip "$tif"

python -c 'if __name__ == "__main__" :
    import sys ; import numpy as np
    import matplotlib.pyplot as plt

    import tifffile as tif

    img = tif.imread( "'"$tif"'" )

    tif.imshow( img )
    plt.show()
'
```
* with OpenCV
```sh
#!/bin/sh

src="empire.jpg"   # ... in pcv_data
tif="empire.tif"

convert "$src" -compress zip "$tif"

cv2_path="/usr/local/opencv/lib/python2.7/dist-packages"
python -c 'if __name__ == "__main__" :
    import sys ; import numpy as np
    sys.path.append( "'"$cv2_path"'" ); import cv2 ; import cv2.cv as cv

    import tifffile as tif

    img = tif.imread( "'"$tif"'" )
    img = cv2.cvtColor( img, cv.CV_RGB2BGR )

    cv2.imshow( "image" , img )
    cv2.waitKey( 0 )
    cv2.destroyWindow( "image" )
'
```
### 03 ... analyze tif
```py
#! /usr/bin/python
#  coding: utf-8

import sys
import numpy as np
import matplotlib.pyplot as plt
import tifffile

fn = "empire.tif"

def analyze ( tif ) :
    print "file size: %d" % tif._fsize
    print "\n" .join( str( p ) for p in tif.pages )

with tifffile.TiffFile( fn ) as tif : analyze( tif )
```
### 04 ... read 1 strip
```py
#! /usr/bin/python
#  coding: utf-8

import sys ; import os.path ; import subprocess
import numpy as np
import matplotlib.pyplot as plt
cv2_path="/usr/local/opencv/lib/python2.7/dist-packages"
sys.path.append( cv2_path ); import cv2 ; import cv2.cv as cv
import tifffile

src = "empire.jpg"  # ... in pcv_data
src_tif  = "empire.tif"
strip_tif = "empire-strip.tif"

if not os.path .exists( src_tif ) :
    subprocess.check_output([
        "convert", src, "-compress", "zip", src_tif ])
if not os.path .exists( strip_tif ) :
    subprocess.check_output([
        "tiffcp", "-s", "-r", "300", src_tif, strip_tif ])

img = tifffile.imread( strip_tif )
img = cv2.cvtColor( img, cv.CV_RGB2BGR )
cv2.imshow( "img", img )

with tifffile.TiffFile( strip_tif ) as tif :
    p = tif.pages[0]
    strip_counts = len( p.tags[ "strip_offsets" ].value )

    for n in range( strip_counts ) :
        strip = p .strip_asarray( n )
        strip = cv2.cvtColor( strip, cv.CV_RGB2BGR )
        cv2.imshow( "strip-%s" % n , strip )
        px = 100 ; py = n * 300 + 100
        cv2.moveWindow( "strip-%s" % n , px, py )

cv2.waitKey( 0 ) ; cv2.destroyAllWindows()
```
### 05 ... read 1 tile
```py
#! /usr/bin/python
#  coding: utf-8

import sys ; import os.path ; import subprocess
import numpy as np
import matplotlib.pyplot as plt
cv2_path="/usr/local/opencv/lib/python2.7/dist-packages"
sys.path.append( cv2_path ); import cv2 ; import cv2.cv as cv
import tifffile

src = "empire.jpg"  # ... in pcv_data
src_tif  = "empire.tif"
tile_tif = "empire-tile.tif"

if not os.path .exists( src_tif ) :
    subprocess.check_output([
        "convert", src, "-compress", "zip", src_tif ])
if not os.path .exists( tile_tif ) :
    subprocess.check_output([
        "tiffcp", "-t", "-w", "400", "-l", "300", src_tif, tile_tif ])

img = tifffile.imread( tile_tif )
img = cv2.cvtColor( img, cv.CV_RGB2BGR )
cv2.imshow( "img", img )

with tifffile.TiffFile( tile_tif ) as tif :
    p = tif.pages[0]
    tile_counts = len( p.tags[ "tile_offsets" ].value )

    for n in range( tile_counts ) :
        tile = p .strip_asarray( n )
        tile = cv2.cvtColor( tile, cv.CV_RGB2BGR )
        cv2.imshow( "tile-%s" % n , tile )
        px = n % 2 * 400 + 100 ; py = n / 2 * 300 + 100
        cv2.moveWindow( "tile-%s" % n , px, py )

cv2.waitKey( 0 ) ; cv2.destroyAllWindows()
```
### 06 ... read BW image
* make BWimage.tif
```sh
#!/bin/sh

gs -dNOPAUSE -dBATCH -sDEVICE=tiffgray -sCompression=lzw -r2400 -q \
   -sOutputFile="Grayimage.tif" "Grayimage.ps"
convert Grayimage.tif -define quantum:polarity=min-is-white BWimage.tif
```
* read BWimage.tif
```py
#! /usr/bin/python
#  coding: utf-8

import sys ; import os.path ; import subprocess
import numpy as np
import matplotlib.pyplot as plt
cv2_path="/usr/local/opencv/lib/python2.7/dist-packages"
sys.path.append( cv2_path ); import cv2 ; import cv2.cv as cv
import tifffile

src_tif  = "BWimage.tif"
print subprocess.check_output([ "tiffinfo", src_tif ])

img = tifffile.imread( src_tif )
print "%s %s" % ( img .dtype , img .shape )

cv2.imshow( "img", 255 - img[ -500:, 1200:2100 ] .astype( "uint8" ) * 255 )
cv2.waitKey( 0 ) ; cv2.destroyAllWindows()
```
